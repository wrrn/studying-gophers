package performers

type Performer interface {
	Perform() string
}
