package fetcher

import (
	"fmt"
	"io/ioutil"
	"net/http"
)

type Fetcher struct {
	URL string
}

func (f Fetcher) Perform() string {
	response, err := http.Get(f.URL)
	if err != nil {
		return fmt.Sprintf("URL: %s\n Error: %v", f.URL, err)
	}

	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		fmt.Sprintf("URL: %s\n Error: Failed to read the response body: %v", f.URL, err)
	}
	return fmt.Sprintf("URL: %s\n Size: %d", f.URL, len(body))
}
