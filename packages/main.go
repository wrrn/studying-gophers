// gitlab.com/wharps/hello
package hello // HL

import "fmt"

func SayHello() {
	fmt.Println("Hello, 🌐")
}
