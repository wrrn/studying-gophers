package main

import "fmt"

func incrementPointer(num *int) {
	*num = *num + 1
}

func incrementValue(num int) {
	num = num + 1
}

func main() {
	i := 5
	pointer := &i
	fmt.Println("Pointer:", pointer)
	fmt.Println("Value:", *pointer)

	incrementValue(i)
	fmt.Println("incrementValue result:", i)
	incrementPointer(&i)
	fmt.Println("incrementPointer result:", i)
}
