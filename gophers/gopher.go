package gophers

import (
	"fmt"
	"math/rand"
	"time"

	"gitlab.com/wharps/studying-gophers/performers"
)

// START STRUCT OMIT
/* OMIT
package gophers
*/ // OMIT

type Gopher struct {
	Name       string
	Experience int
	secret     string
}

// END STRUCT OMIT

// START DEFINE OMIT
var (
	Goldy = Gopher{
		Name:       "Goldy",
		Experience: 16,
		secret:     "sketchy secret",
	}
	Phil = Gopher{
		Name:       "Phil",
		Experience: 4,
		secret:     "dark secret",
	}
	Chuck = Gopher{"Chuck", 19, "no secret"}
)

// END DEFINE OMIT

// START LIST OMIT
var GSlice = make([]Gopher, 0, 3)

var gophers = []Gopher{
	Phil,
	Goldy,
	Chuck,
}

// END LIST OMIT

// START MAP OMIT
var GMap = make(map[string]Gopher)

var GopherMap = map[string]Gopher{
	"Phil":  Phil,
	"Goldy": Goldy,
	"Chuck": Chuck,
}

// END MAP OMIT

// START BASIC FUNCTION OMIT

func PrintName(g Gopher) {
	fmt.Println(g.Name)
}

func GetSecret(g Gopher) (int, string) {
	runes := []rune(g.secret)
	i := rand.Intn(len(runes))
	// Split it in two and swap
	runes = append(runes[i+1:], runes[:i]...)

	return i, string(runes)
}

// END BASIC FUNCTION OMIT

/* START NAMED RETURN OMIT
func GetSecret(g Gopher) (index int, cipher string) {
	runes := []rune(g.secret)
	index = rand.Intn(len(runes))
	// Split it in two and swap
	runes := append(runes[i+1:], runes[:i]...)

	return i, string(runes)
}
*/ // END NAMED RETURN OMIT

/* START GET VALUES OMIT
count, secret := GetSecret(Chuck)
*/ // END GET VALUES OMIT

// START FUNCTION TYPE OMIT
type GopherFunc func(g Gopher)

func ForEach(fn GopherFunc, gs ...Gopher) {
	defer fmt.Println("All Done")

	for _, gopher := range gs {
		fn(gopher)
	}
}

// END FUNCTION TYPE OMIT

// BASIC METHOD OMIT
func (g Gopher) Introduce() {
	fmt.Println("Hello, my name is", g.Name)
}

func (g Gopher) Secret() string {
	return g.secret
}

func (gfn GopherFunc) Profile(g Gopher) string {
	fmt.Println("===== Entering Profile =====")
	start := time.Now()
	gfn(g)
	fmt.Println("===== Finishing Profile =====")
	return fmt.Sprintf("%s", time.Since(start))
}

// END BASIC METHOD OMIT

// START POINTER OMIT
func (g Gopher) EphemeralSecret(secret string) {
	g.secret = secret
}

func (g *Gopher) SetSecret(secret string) {
	g.secret = secret
}

// END POINTER OMIT

/* BEGIN PERFORM OMIT
// package gophers
func (g Gopher) MeasurePerformance(performer performers.Perform) (string, time.Duration) {
	start := time.Now()
	result := performer.Perform()
	return result, time.Since(start)
}
*/ // END PERFORM OMIT

// BEGIN ERRORS OMIT
type GopherError struct {
	gopher Gopher
	msg    string
}

func (g GopherError) Error() string {
	return fmt.Sprintf("%s: %s", g.gopher.Name, g.msg)
}

func (g Gopher) MeasurePerformance(performer performers.Performer) (string, time.Duration, error) {
	start := time.Now()
	result := performer.Perform()
	if len(result) == 0 {
		return "", 0, GopherError{gopher: g, msg: "task returned an empty result"}
	}
	return result, time.Since(start), nil
}

// END ERRORS OMIT
