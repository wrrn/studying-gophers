package main

import (
	"fmt"

	. "gitlab.com/wharps/studying-gophers/gophers"
)

func main() {
	fmt.Println("Phil's original secret:", Phil.Secret())

	Phil.EphemeralSecret("yet another secret")
	fmt.Println("By Value:", Phil.Secret())

	(&Phil).SetSecret("I wish I was using rust")
	fmt.Println("By reference:", Phil.Secret())

	(&Phil).EphemeralSecret("super secret")
	fmt.Println("&Phil's ephemeral secret:", Phil.Secret())

	Phil.SetSecret("I don't have any other secrets")
	fmt.Println("Phil's secret:", Phil.Secret())
}
