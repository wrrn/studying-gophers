package main

import (
	"fmt"
	"time"

	"gitlab.com/wharps/studying-gophers/fetcher"
	"gitlab.com/wharps/studying-gophers/gophers"
	"gitlab.com/wharps/studying-gophers/performers"
	"gitlab.com/wharps/studying-gophers/quote"
)

func main() {
	tasks := []performers.Performer{
		fetcher.Fetcher{URL: "https://golang.org"},
		fetcher.Fetcher{URL: "https://www.rust-lang.org/"},
		quote.Generator{Src: quote.Quotes},
	}
	defer TotalTime(time.Now())

	for i, gopher := range []gophers.Gopher{gophers.Phil, gophers.Goldy, gophers.Chuck} {
		result, duration, err := gopher.MeasurePerformance(tasks[i])
		if err != nil {
			fmt.Printf("Error: %v\n", err)
			return
		}
		fmt.Printf("%s\n Time: %v\n\n", result, duration)
	}
}

// END MAIN OMIT
func TotalTime(start time.Time) {
	fmt.Println("Total time:", time.Since(start))
}
