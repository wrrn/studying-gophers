package fail

import (
	"math/rand"
	"time"

	"gitlab.com/wharps/studying-gophers/performers"
)

type Sometimes struct {
	performers.Performer
}

func (s Sometimes) Perform() string {
	rand.Seed(time.Now().Unix())
	if rand.Int()%2 == 0 {
		return ""
	}

	return s.Performer.Perform()
}
