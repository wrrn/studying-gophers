package hello

import "fmt"

var World = "🌐" // HL

func SayHello() {
	greeting := "Hello" // HL
	fmt.Printf("%s, %s", greeting, World)
	greeting = "Goodbye" // HL
}
