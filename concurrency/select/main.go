package main

import (
	"fmt"
	"time"

	"gitlab.com/wharps/studying-gophers/fail"
	"gitlab.com/wharps/studying-gophers/fetcher"
	"gitlab.com/wharps/studying-gophers/gophers"
	"gitlab.com/wharps/studying-gophers/performers"
	"gitlab.com/wharps/studying-gophers/quote"
)

var tasks = []performers.Performer{
	fetcher.Fetcher{URL: "https://golang.org"},
	fetcher.Fetcher{URL: "https://www.rust-lang.org/"},
	fail.Sometimes{quote.Generator{Src: quote.Quotes}},
}

func TotalTime(start time.Time) {
	fmt.Println("Total time:", time.Since(start))
}

func main() {
	// -- snip -- //
	defer TotalTime(time.Now())
	errors := make(chan error) // HL
	results := make(chan string)
	for i, gopher := range []gophers.Gopher{gophers.Phil, gophers.Goldy, gophers.Chuck} {
		go func(g gophers.Gopher, task performers.Performer, c chan<- string, e chan<- error) {
			result, duration, err := g.MeasurePerformance(task)
			if err != nil {
				e <- err // HL
				return   // HL
			}
			c <- fmt.Sprintf("%s is finished:\n %s\n Time: %s\n\n", g.Name, result, duration)
		}(gopher, tasks[i], results, errors)
	}

	for i := len(tasks); i > 0; i-- {
		select { // HL
		case result := <-results: // HL
			fmt.Println(result)
		case err := <-errors: // HL
			fmt.Printf("Error: %v\n\n", err) // HL
		}
	}
}
