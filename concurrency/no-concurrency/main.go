package main

import (
	"fmt"
	"net/http"
	"time"
)

type Language struct {
	Name string
	URL  string
}

var langs = []Language{
	{
		Name: "Go",
		URL:  "https://golang.org",
	},
	{
		Name: "Java",
		URL:  "https://java.com",
	},
	{
		Name: "PHP",
		URL:  "https://php.net",
	},
}

// BEGIN SHOW OMIT
func get(l Language) {
	start := time.Now()
	http.Get(l.URL)
	fmt.Printf("%s: %03f\n", l.Name, time.Since(start).Seconds())
}

func main() {
	for _, l := range langs {
		go get(l)
	}
	time.Sleep(5 * time.Second)

}

// END SHOW OMIT
