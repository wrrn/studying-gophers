package main

import (
	"fmt"
	"time"

	"gitlab.com/wharps/studying-gophers/fail"
	"gitlab.com/wharps/studying-gophers/fetcher"
	"gitlab.com/wharps/studying-gophers/gophers"
	"gitlab.com/wharps/studying-gophers/performers"
	"gitlab.com/wharps/studying-gophers/quote"
)

var tasks = []performers.Performer{
	fetcher.Fetcher{URL: "https://golang.org"},
	fetcher.Fetcher{URL: "https://www.rust-lang.org/"},
	fail.Sometimes{quote.Generator{Src: quote.Quotes}},
}

func TotalTime(start time.Time) {
	fmt.Println("Total time:", time.Since(start))
}

func main() {
	// -- snip -- //
	defer TotalTime(time.Now())

	results := make(chan string) // HL
	for i, gopher := range []gophers.Gopher{gophers.Phil, gophers.Goldy, gophers.Chuck} {
		go func(g gophers.Gopher, task performers.Performer, c chan<- string) { // HL
			result, duration, err := g.MeasurePerformance(task)
			if err != nil {
				fmt.Printf("Error: %v\n", err)
			}
			c <- fmt.Sprintf("%s is finished:\n %s\n Time: %s\n\n", g.Name, result, duration) // HL
		}(gopher, tasks[i], results)
	}

	for i := len(tasks); i > 0; i-- {
		result := <-results // HL
		fmt.Println(result) // HL
	}
}
