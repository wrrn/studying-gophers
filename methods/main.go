package main

import . "gitlab.com/wharps/studying-gophers/gophers"

func main() {
	// SHOW CALLS OMIT
	var PName GopherFunc = PrintName
	Phil.Introduce()
	PName.Profile(Phil)
	// END SHOW CALLS OMIT
}
