package hello

import "fmt"

var World = "🌐"               // HL
var defaultGreeting = "Hello" // HL

func SayHello() {
	fmt.Printf("%s, %s", defaultGreeting, World)
}

func say() { // HL
	// -- snip --//
}
