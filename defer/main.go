package main

import (
	"fmt"
	"time"
)

func TotalTime(start time.Time) {
	fmt.Println("Total Time:", time.Since(start))
}

func main() {
	defer TotalTime(time.Now())
	defer fmt.Println("About to return")
	fmt.Println("About to sleep")
	time.Sleep(time.Second * 5)
	fmt.Println("Woke up")
}
