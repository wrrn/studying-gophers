package quote

import (
	"fmt"
	"math/rand"
)

type Quote struct {
	quote  string
	author string
}

var Quotes = []Quote{
	{"Never go to bed mad. Stay up and fight.", "Phyllis Diller"},
	{"Accept who you are. Unless you're a serial killer.", "Ellen DeGeneres"},
	{"I did not attend his funeral, but I sent a nice letter saying I approved of it.", "Mark Twain"},
	{"They love their hair because they're not smart enough to love something more interesting.", "John Green"},
	{"If a book about failures doesn't sell, is it a success?", "Jerry Seinfeld"},
}

type Generator struct {
	Src []Quote
}

func (g Generator) Perform() string {
	quote := g.Src[rand.Intn(len(g.Src))]
	return fmt.Sprintf("%s --%s", quote.quote, quote.author)
}
